import math


def point_distance(x1, x2, y1=12, y2=10):   # 1 задание
    z = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    print("Pасстояние между двумя точками:")
    print(z)


def temperature(c):    # 2 задание
    f = c*9/5+32
    print("Температура по Фаренгейту:")
    print(f)


point_distance(1, 5, 2, 8)  # 3 задание: используя позиц. арг.
temperature(c=5)            # используя имена пар-ов
point_distance(5, 11)       # 4 задание
point_distance(1, 5, 6)


def square(r):              # 5 задание
    s = math.pi*r**2
    print("Площадь круга:")
    print(s)


a = 7
b = 5


square((lambda a, b: a % b)(a, b))
