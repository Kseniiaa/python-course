# 3 завдання
file = open("file1.txt", "w") # записуємо рядки в файл
file.write("O, speak again, bright angel, for thou art \n\
As glorious to this night, being o'er my head, \n\
As is a winged messenger of heaven \n\
Unto the white-upturned wondering eyes \n\
Of mortals that fall back to gaze on him \n\
When he bestrides the lazy-puffing clouds \n\
And sails upon the bosom of the air.")

# 4 завдання
def change_strings(s): # функция для замены строк начинающихся с 'As' на пустые строки
    file2 = open("file2.txt", "a")
    global count
    count = 0
    if s.startswith('As'):
        count += 1
        s = s.replace(s, "\n")
        file2.write(s)
    else:
        file2.write(s)


file = open("file1.txt", "r")

for str in file:
    change_strings(str) # вызов функции

# 5 завдання
print("quantity of strings starting with 'As': ", count) # output: 2

# 6 завдання
user_answer = input("Enter 'file1' or 'file2' to see files or 'exit': ")
while user_answer != "exit":

    if user_answer == "file1":
        file = open("file1.txt", "r")
        print (f"File 1 containment:\n{file.read()}") # показ першого файлу
        user_answer = input("enter name of file: ")

    elif user_answer == "file2":
        file2 = open("file2.txt", "r")
        b = ""
        for s in file2:
            if s.startswith("\n"):
                continue
            else:
                b += s
        print (b) # показ другого файлу
        user_answer = input("enter name of file: ")

    else:
        print("WRONG NAME") # якщо неправильно введено назву файлу
        user_answer = input("enter 'file1' or 'file2' to see the files: ")