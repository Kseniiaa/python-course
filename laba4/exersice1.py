import math
# -*- coding: utf-8 -*-
def length(r):
    p = 2 * math.pi * r
    print('%s %d %s %f' % ('Довжина кола з радіусом ', r, 'дорівнює', p))  # старий стиль
    print('Довжина кола з радіусом {} дорівнює {}'.format(r, p))  # новий стиль

user_response = input("Введіть радіус кола:")
radius = int(user_response)
length(radius)